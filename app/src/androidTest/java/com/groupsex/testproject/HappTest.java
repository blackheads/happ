package com.groupsex.testproject;

import android.content.res.Resources;
import android.test.ActivityInstrumentationTestCase2;

import com.groupsex.testproject.repository.Quiz;
import com.robotium.solo.Solo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Babak on 2015-05-28.
 */
public class HappTest extends ActivityInstrumentationTestCase2<MainActivity> {

private Solo solo;
        ArrayList<Quiz> quizList = new ArrayList<>();
        ArrayList<Quiz> quizList2 = new ArrayList<>();
        ArrayList<Quiz> quizList3 = new ArrayList<>();
        ArrayList<Quiz> questionListOne = new ArrayList<>();
        ArrayList<Quiz> questionListTwo = new ArrayList<>();
        ArrayList<Quiz> questionListThree = new ArrayList<>();
        InputStream input;
        InputStream input2;
        InputStream input3;

    public HappTest() {
        super(MainActivity.class);
        }

    public void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation());
        getActivity();
        Resources testRes = getActivity().getResources();
        input = testRes.openRawResource(R.raw.quiz_walk_questions);
        input2 =  testRes.openRawResource(R.raw.quiz_walk_questions2);
        input3 =  testRes.openRawResource(R.raw.quiz_walk_questions3);

        }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
        }

    public void testAppClickable() {
        //Wait for activity: 'com.groupsex.testproject.MainActivity'
        solo.waitForActivity(MainActivity.class, 2000);
        //Click on Hur spelar man ?
        solo.clickOnText("Hur spelar man?");
        //Click on ok
        solo.clickOnText("OK");
        //Click on GÅ I HÅKANS FOTSPÅR
        solo.clickOnView(solo.getView(R.id.cta_button));
        //Wait for activity: 'com.groupsex.testproject.FootsparActivity'
        assertTrue("com.groupsex.testproject.FootsparActivity is not found!", solo.waitForActivity(FootsparActivity.class));
        //Click on Alternativ 1
        assertNotNull(R.id.btnAlt_1);
        solo.clickOnView(solo.getView(R.id.btnAlt_1));
        //Click on Alternativ 2
        assertNotNull(R.id.btnAlt_2);
        solo.clickOnView(solo.getView(R.id.btnAlt_2));
        //Click on Alternativ 3
        assertNotNull(R.id.btnAlt_3);
        solo.clickOnView(solo.getView(R.id.btnAlt_3));
        //Click on HomeView TIPSPROMENAD
        solo.clickOnText("TIPSPROMENAD");
        //Wait for activity: 'com.groupsex.testproject.MainActivity'
        assertTrue("com.groupsex.testproject.MainActivity is not found!", solo.waitForActivity(MainActivity.class));
        }

    public void testTxtFilesComplete(){
        assertEquals(questionListOne, quizList);
        assertEquals(questionListTwo, quizList2);
        assertEquals(questionListThree, quizList3);
    }


    public void testRefactorTextFilesToQuiz() {

        try {
            quizList = refactorTxtFileToQuiz(input);
            quizList2 = refactorTxtFileToQuiz(input2);
            quizList3 = refactorTxtFileToQuiz(input3);
        } catch (IOException e) {
            e.getMessage();
        }
        assertNotNull(quizList);
        assertNotNull(quizList2);
        assertNotNull(quizList3);
    }

   public  ArrayList<Quiz> refactorTxtFileToQuiz(InputStream input) throws IOException {
       ArrayList<Quiz> qList = new ArrayList<>();
       InputStreamReader inputReader = null;
       BufferedReader bufferedReader;
       String line;
           inputReader = new InputStreamReader(input);
           bufferedReader = new BufferedReader(inputReader);
           while ((line = bufferedReader.readLine()) != null) {
               String[] quizDetails = line.split("-");
               Quiz quiz = new Quiz();
               quiz.setTitle(quizDetails[0]);
               quiz.setQuestion(quizDetails[1]);
               quiz.setAlt1(quizDetails[2]);
               quiz.setAlt2(quizDetails[3]);
               quiz.setAlt3(quizDetails[4]);
               quiz.setAnswer(quizDetails[5]);
               quiz.setOnAnswer(quizDetails[6]);
               qList.add(quiz);
       }
       return qList;
   }

    public void testBuildMockTxtFile() {

        String[] questions = new String[14];
        questions[0] = "Paviljong-Från skivan 'För sent för Edelweiss' sjunger Håkan om denna plats. Här kan man äta eller ta en fika med en utsikt över Göteborgs hamninlopp. Vad heter låten där han sjunger om denna plats?-Kär i en Ängel-Långa vägar-Tro och tvivel-Tro och tvivel-Tro och  tvivel var en stor hit av Håkan, det är även låten där han sjunger om sin uppväxt. Ett utav platserna han sjunger om är Paviljongen. 'Det var nästan alltid svart i tankarna på Långedragspaviljongen där jag nästan alltid satt med pensionärerna under båtsäsongen och vi såg på de unga och glömska, i båtar och i varandras armar och tänkte 'skuld och lidande väntar på er fortfarande.";
        questions[1] = "Hakansuppvaxt-I detta område, Högsbohöjd, bodde Håkan under sin uppväxt. Vilket år föddes Håkan Hellström?-1970-1972-1974-1972-Fastän det känns som Håkan har funnits i evigheter så föddes han 1972.";
        questions[2] = "Dalaskolan-Håkan Hellström slutade Dalaskolan i Ekebäck efter årskurs 6. Vilken skola började Håkan i efteråt?-Samskolan-Vättleskolan-Vasaskolan-Samskolan-Håkan gick i Dalaskolan fram till årskurs 6 därefter började Håkan i Samskolan.";
        questions[3] = "Ullevi-Håkan Hellström klev upp på ett fullsatt Ullevi i Göteborg, 2014. Hur många besökare var det på konserten?-71.244-69.349-59.488-69.349-Den 7 Juni 2014 skrevs svensk musikhistoria, 69.349 var på plats i Ullevi.";
        questions[4] = "Valvet-I låten 'Dom där jag kommer ifrån' från skivan '2 steg från paradise' sjunger håkan om valvet scenen. Det var där han för första gången såg en speciell person. Vad heter personen?-Erik Johansson-Lena Pam-Freddie Wadling-Freddie Wadling-Freddie Wadling är en sångare, skådespelare och opera skrivare med över 30 års erfarenhet inom musik branschen.";
        questions[5] = "Slottskogsvallen-2013 hade Håkan en turné, när den nådde Göteborg stod över 20 000 fans i Slottsskogsvallen för att se Håkan i sin hemstad. Vilken månad var Håkan i Slottskogsvallen?-Juni-Maj-Oktober-Juni-Över 20 000 Göteborgare samlades för att se Håkan som en start på sommaren i Juni 2013.";
        questions[6] = "Samskolan-Håkan var med och bildade bandet Brodel Daniel under skolåren, då han, Henrik Berggren med fler gick på Göteborgs högre samskola. Vilken roll hade Håkan i bandet?-Sångare-Trummis-Gitarrist-Trummis-Många utav er trodde säkert att han höll endast på med sång, men innan han blev en sångare så var han faktist en trummis.";
        questions[7] = "Allen-Nya Allén är en parkgata i Göteborg omsjungen av storheter som Nationalteatern och Håkan. I vilken sång nämner Håkan allén?-Zigernaliv dreamin-Livets teater-2 steg från paradise-Zigernaliv dreamin-Nya Allén stod klar 1823. 1 000 träd planterades längs gatan.I dag, knappt 200 år senare, är Nya Allén, Trädgårdsföreningen och Kungsparken de mest populära parkerna i Göteborg. 'Zigernaliv dreamin~Tar en sväng ner mot allén sätter mig en stund under träden allt är sig likt här sen Nationalteatern köper basiado av en kara på torget. Och går hem och lägger mig på sängen och läser Beppe Wolgers.'";
        questions[8] = "Mug-Musik utan gränser, en av de mest populära musikaffärerna i Göteborg. Håkan Hellström livespelade här år 2008. Vad gjorde Håkan Hellström under sin skolgång i Mug?-Musik lektioner-Träffade en sång coach-Praktiserade-Praktiserade-Redan under tidig ålder så var Håkan intresserad av musik & instrument. Drömmen var att arbeta inom musikbranschen. Under skolgången valde han att praktisera på Mug.";
        questions[9] = "Feskekorka-Feskekörka, där den står idag invigdes år 1874 och är platsen för dig som gillar fisk och skaldjur. Omtalad i Håkans texter och en av de bästa scenerna från filmen 'Känn ingen sorg' som utspelar sig bakom körkan. Vart i Feskekörka är filmscenen?-Vid hamnen-I en container-På en brygga-I en container-På Feskekörka har det sålts fisk i över 140 år. Idag så är det inte bara en fisk marknad det finns även ett skaldjur restaurang i bygnaden.I ena hörnet av kyrkan så finns det en container där filmscenen utspelades.";
        questions[10] = "Stibergsliden-Stigbergsliden sträcker sig ca 350m från Johannesplatsen upp till Stigbergstorget och nämns i en utav Håkans låtar från albumet 'För sent för Edelweiss'.I Vilken låt i albumet nämns Stigbergsliden?-Långa vägar-Zigenarliv Dreamin'-Dom fyra årstiderna-Zigenarliv Dreamin'-Det första Håkan sjunger i låten Zigenarliv Dreamin är 'En vacker dag på Stigbergsliden ser upp och ser en fågel o tänker 'din jävel'. Stigbergsliden är en gata i Majorna, en förbindelseled mellan Masthugget och Majorna.'";
        questions[11] = "Gullbergskaj-Kajen belägen i stadsdelen Gullbergsvass, är en plats som Håkan håller kär. Låten 'Mitt Gullbergs kaj paradis' är inspirerad från denna plats. Kajen benämns också som Drömmarnas kaj. Varför kallas platsen Drömmarnas kaj?-Platsen är ej moderniserat-Platsen är vacker-Platsen har största hamnen i Göteborg-Platsen är ej moderniserat-Det vackra med denna plats är att den har stått stilla i flera år. Håkan sjunger om att platsen inte är moderniserad. Där ligger en mängd äldre fartyg under renovering. Längs kajen finns även ett flytande hotell. 'I hela mitt liv, hela mitt liv det känns som jag hör hemma i Gullbergs Kaj paradis och känn dig inte skyldig om du möter mig där.'";
        questions[12] = "Bageriet-I albumet 'För Sent För Edelweiss' i låten 'Tro och Tvivel' sjunger håkan om bageriet. Vilket år släpptes albumet?-1998-2008-1994-2008- 2008 släpptes albumet. 1994 så arbetade håkan på bageriet tills han blev en trummis i bandet 'Honey Is Cool' från 1995 till 1997. 'Tro och Tvivel '94 hade jag ett fast jobb på Pååls bagerier Till jag började med Tequila och blev trummis i en ny orkester.'";
        questions[13] = "Jazzhuset-'Ja, Jazzhuset är fullt med trix och cocacola chicks och kicks!'. Detta nämner Håkan i hitlåten 'Kom igen Lena'. Jazzhuset är en viktig del i Göteborgs nöjesliv sedan 1977. Tidigare var det enbart en Jazzklubb men nuförtiden så huserar allt från Indiepop, garagerock synt och electronica på klubbarna. Vilket år släpptes låten 'Kom igen Lena'?-2001-2002-2003-2002-'Kom igen Lena' är en klassiker av Håkan som slog igenom 2002 från albumet 'Det är så jag säger det'.";
        assertNotNull(questions);
        assertTrue(questions.length == 14);
        for (int i = 0; i < questions.length; i++) {
            String question[] = questions[i].split("-");
            Quiz quiz = new Quiz();
            quiz.setTitle(question[0]);
            quiz.setQuestion(question[1]);
            quiz.setAlt1(question[2]);
            quiz.setAlt2(question[3]);
            quiz.setAlt3(question[4]);
            quiz.setAnswer(question[5]);
            quiz.setOnAnswer(question[6]);
            questionListOne.add(quiz);
        }
        assertNotNull(questionListOne);
    }


    public void testBuildMockTxtFileTwo() {

        String[] questions = new String[14];
        questions[0] = "Paviljong-I låten 'Tro och tvivel' sjunger Håkan om denna plats. Här på Paviljongen så har man en grym utsikt över Göteborgs hamninlopp, det är en utav anledningarna till att Håkan återkom till denna plats. Låten 'Tro och tvivel' som är en utav Håkans mest spelade låtar släpptes år 2008. Vilken skiva innehåller låten?-För sent för Edelweiss-Luften bor i mina steg-2 Steg från Paradise-För sent för Edelweiss-Skivan 'För sent för Edelweiss' som släpptes år 2008 tog Håkan fans med storm, fortfarande spelas albumet på radio. Låten 'Tro och tvivel' är en utav den mest populära låten i albumet 'För sent för Edelweiss'.";
        questions[1] = "Hakansuppvaxt-Håkan Hellström bodde i Högsbohöjd under sin barndoms uppväxt. Hur gammal var Håkan Hellström när han flyttade till centrala Göteborg tillsammans med sin familj?-11 år-12 år-13 år-13 år-Håkan flyttade med sin familj till centrala Göteborg när han började Samskolan detta gjorde dem år 1985.";
        questions[2] = "Dalaskolan-Håkan Hellström studerade i Dalaskolan fram till årskurs 6. Därefter så började Håkan i Samskolan. Under vilka år studerade Håkan i Dalaskolan?-1979 1986-1982 1989-1980 1987-1980 1987-Håkan föddes år 1974 när han fyllde 6 år så började han förskola i Dalaskolan fram till årskurs 6 därefter flyttade han och hans familj till centrala Göteborg och Håkan började i Samskolan.";
        questions[3] = "Ullevi-Håkan Hellström som klev upp på ett fullsatt Ullevi med nästan 70.000 i publiken, vilket  gjorde det till en utav den största spelningen i Sverige någonsin. Vilket år uppträde Håkan i Ullevi?-2013-2014-2015-2014-Som en start på sommaren 2014 så klev Håkan upp på scenen den 7 juni. Rekordet med nästan 70.000 i publiken gjorde det till en utav den mest historiska spelningen i Sverige någonsin.";
        questions[4] = "Valvet-I en utav Håkan Hellströms kändaste låtar så sjunger Håkan om valvet scenen. Låten kommer från albumet '2 Steg från Paradise' som släpptes år 2010. Vad heter låten där Håkan sjunger om valvet scenen?-Dom där jag kommer ifrån-Det här är min tid-Shelley-Dom där jag kommer från-'Första gången jag såg Freddie var på valvetscenen, Vi ska ha ett band som fan låter exakt som Liket Lever' Texten är tagen från låten 'Dom där jag kommer från' en klassiker från Håkan Hellström där han nämner valvet scenen.";
        questions[5] = "Slottskogsvallen-Håkan var på turné sommaren 2013, när den nådde Göteborg så stod ett antal fans här på Slottsskogsvallen för att se Håkan i sin hemstad. Hur många fans var på plats?-20 000-25 000- 30 000-20 000-Som en invigning på sommaren så samlades 20 000 Göteborgare här för att se sin stora idol. Håkan Hellström som hade sin första Göteborg spelning här på Slottskogsvallen attraherade massvis med fans.";
        questions[6] = "Samskolan-Håkan Hellström var med och bildade ett band under sina skolår i Göteborgs högre Samskola. Han började i Samskolan årskurs 7. Vilken skola studerade Håkan i innan Samskolan?-Dalaskolan-Vasaskolan-Montessoriskolan-Dalaskolan-Håkan studerade I Dalaskolan fram till årskurs 6 då skolan inte har fler årskurser. Han började därefter i Samskolan där han var med och bildade bandet Broder Daniel.";
        questions[7] = "Allen-Nya Allén en stor parkgata i Göteborg omsjungen av storheter som Håkan Hellström. I låten 'Zigernaliv dreamin' nämner håkan Nya Allén 'Zigernaliv dreamin~Tar en sväng ner mot allén sätter mig en stund under träden allt är sig likt här sen Nationalteatern köper basiado av en kara på torget. Och går hem och lägger mig på sängen och läser Beppe Wolgers.'. När byggdes Allén?-1886-1930-1823-1823-Allén en gata som sträcker sig cirka 1800 meter i centrala Göteborg. Allén stod klar år 1823. Idag knappt 200 år sedan är Allén den mest populära parken i Göteborg.";
        questions[8] = "Mug-Musik utan gränser, som är en utav de mest populära musikaffärerna i Göteborg. Håkan praktiserade här under sin skolgång i Samskolan. Under hans karriär så har han haft ett par minnesvärda spelningar, en utav dom spelades här på musik utan gränser. Vilket år uppträde Håkan Hellström i musik utan gränser?-2007-2008-2009-2008-År 2008 under Håkan Hellströms 24 gig turné så var ett utav stoppen här på mug, samma år som han släppte albumet 'För sent för edelweiss'.";
        questions[9] = "Feskekorka-Feskekörka är en fisk marknad som har sålt fisk i över 140 år, där finns även en skaldjursrestaurang . Vid ena hörnet av kyrkan så finns det en container där en utav de bästa scenerna från filmen ’’känn ingen sorg’’ utspelades och är omtalad i Håkans texter.  Vilket år invigdes Feskekörka?-1870-1872-1874-1874-Feskekörka invigdes 1 november 1874. Den ritades av Göteborgs dåvarande stadsarkitekt, Victor von Gegerfelt. Med de karaktäristiska, spetsbågade fönstren och med en konstruktion utan mellanväggar eller pelare var Feskekörka en för sin tid futuristisk byggnad.";
        questions[10] = "Stibergsliden-Stigbergsliden nämns i en utav Håkans låtar från albumet 'För sent för Edelweiss'.I albumet finns låten ’’Zigenarliv Dreamin’’Det första Håkan sjunger i låten Zigenarliv Dreamin är 'En vacker dag på Stigbergsliden ser upp och ser en fågel o tänker 'din jävel'. Stigbergsliden är en gata i Majorna, en förbindelseled mellan Masthugget och Majorna.' Vad heter den plats som stigberssliden slutar vid.-Stigbergsparken-Stigbergstorget-Stigberget-Stigbergstorget-Stigbergstorget är ett torg i stadsdelen Majorna i Göteborg. En mindre del av torgets östra del i 4:e kvarteret Tröstekällan, tillhör stadsdelen Stigberget.";
        questions[11] = "Gullbergskaj-Kajen belägen i stadsdelen Gullbergsvass, är en plats som Håkan håller kär. Låten 'Mitt Gullbergs kaj paradis' är inspirerad från denna plats. Kajen benämns också som Drömmarnas kaj. Platsen kallas ’’Drömmarnas kaj’’ för att platsen ej är moderniserad. Det vackra med denna plats är att den har stått stilla i flera år. Håkan sjunger om att platsen inte är moderniserad. Där ligger en mängd äldre fartyg under renovering. Längs kajen finns även ett flytande hotell.  Vad sjunger Håkan i texten? ’’I hela mitt liv, hela mitt liv det känns som jag hör hemma i Gullbergs Kaj paradis och känn dig inte skyldig…”-Till att möta mig där-Om du möter mig Där-När du möter mig där-Om du möter mig där-'Och känn dig inte skyldig om du möter mig där'. Texten är tagen ur låten 'Mitt Gullbergs Kaj Paradis' där han nämner Gullbergskajen.";
        questions[12] = "Bageriet-I albumet 'För Sent För Edelweiss' sjunger Håkan om ett bageri. Albumet släpptes år 1994. Håkan arbetade på bageriet tills han blev en trummis i bandet 'Honey Is Cool' från 1995 till 1997. Vad heter låten som Håkan sjunger år 1994.-Tro och tvivel-Tro och tvek- Tro och tvivla-Tro och tvivel-'Håkan: år 1994 hade jag ett fast jobb på Pååls bagerier Till jag började med Tequila och blev trummis i en ny orkester.' Texten är tagen från låten 'Tro och tvivel' där han sjunger om sin uppväxt och en del utav den var här på Bageriet.";
        questions[13] = "Jazzhuset-'Ja, Jazzhuset är fullt med trix och cocacola chicks och kicks!'. Detta nämner Håkan i hitlåten 'Kom igen Lena'. Jazzhuset är en viktig del i Göteborgs nöjesliv sedan 1977. Tidigare var det enbart en Jazzklubb men nuförtiden så huserar allt från Indiepop, garagerock synt och electronica på klubbarna. 'Kom igen Lena' är en klassiker av Håkan som slog igenom år 2002 från vilket album?-Det är så jag säger det-Det är så jag sjunger det-Det är så vi säger det-Det är så jag säger det-En längre låt av Håkan knappt 5 minuter lång 'Det är så jag säger det' så sjunger Håkan om kärleken i hans liv.";
        assertNotNull(questions);
        assertTrue(questions.length == 14);

        for (int i = 0; i < questions.length; i++) {
            String question[] = questions[i].split("-");
            Quiz quiz = new Quiz();
            quiz.setTitle(question[0]);
            quiz.setQuestion(question[1]);
            quiz.setAlt1(question[2]);
            quiz.setAlt2(question[3]);
            quiz.setAlt3(question[4]);
            quiz.setAnswer(question[5]);
            quiz.setOnAnswer(question[6]);

            assertNotNull(quiz);

            questionListTwo.add(quiz);
        }
        assertNotNull(questionListTwo);
    }

    public void testBuildMockTxtFileThree() {

        String[] questions = new String[14];
        questions[0] = "Paviljong-I låten 'Tro och tvivel' från skivan 'För sent för Edelweiss' sjunger Håkan om denna plats. Här kan man äta eller ta en fika med en utsikt över Göteborgs hamninlopp. Vilket år släpptes låten?-2005-2008-2010-2008-Året 2008 släppte Håkan albumet 'För sent för Edelweiss'. En utav de mer populära låtarna i albumet är låten 'Tro och tvivel'.";
        questions[1] = "Hakansuppvaxt-I detta område, Högsbohöjd, bodde Håkan under sin uppväxt. Håkan föddes år 1974 och bodde här fram tills han blev 13 år gammal. Håkan tillsammans med sin familj flyttade in till centrala Göteborg 1985. Hur många syskon har Håkan Hellström?-Ett-Två-Fyra-Ett-Håkan som bodde i Högsbohöjd tillsammans med sin far, mor och sin äldre bror Tomas. Tomas och Håkan har inga fler syskon men idag så har Håkan en son som föddes 2005.";
        questions[2] = "Dalaskolan-Här i Ekebäck så studerade Håkan Hellström i Dalaskolan fram till årskurs 6. Därefter så började Håkan i Samskolan. Varför slutade Håkan Dalaskolan?-Dalaskolan har endast 6 årskurser-Håkan startade ett band-Hans familj flyttade-Dalaskolan har endast 6 årskurser-Dalaskolan som än idag endast har årskurser fram till 6:an, gjorde att Håkan tog 'examen' i skolan och fortsatte högstadiet i Samskolan.";
        questions[3] = "Ullevi-Håkan Hellströms succékonsert på Ullevi sommaren 2014 innebar ett nytt svenskt publikrekord. Det sägs att det var 69 349 åskådare på plats i Göteborg och artisten hyllades som profet i sin egen hemstad efter Nordens största konsert genom tiderna. Hur länge varade konserten?-3 Timmar-5 Timmar-4 Timmar-3 Timmar-Trots att 3 timmar låter mycket så beskrev fansen det som de kvickaste 3 timmarna i deras liv på grund av hur kul de hade.";
        questions[4] = "Valvet-I låten 'Dom där jag kommer från' från skivan '2 steg från Paradise' sjunger Håkan om valvet scenen. 'Första gången jag såg Freddie var på valvetscenen, Vi ska ha ett band som fan låter exakt som Liket Lever'. Vilket år släpptes albumet  '2 steg från Paradise'?-2004-2006-2010-2010-Låten 'Dom där jag kommer från' i samband med albumet '2 steg från Paradise' släpptes 2010 en massiv hit som än idag är populär. Håkans fans brukar anse att detta är hans bästa album hittills.";
        questions[5] = "Slottskogsvallen-En vacker sommar åkte Håkan på turné, turnépremiären som var här på Slottskogsvallen fick över 20 000 fans att samlas för att se sin stora idol. Vilket år var Håkan på turné?-2010-2013-2005-2013-Juni 2013 åkte Håkan på stor turné runt om i Sverige. Turné premiären var här på Slottskogsvallen. Hela 26 spelningar åkte Håkan på under sin turné och han sa efteråt att han skulle orka med 20 stopp till.";
        questions[6] = "Samskolan-Håkan började i Samskolan efter Dalaskolan då Dalaskolan inte lär ut mer efter årskurs 6. När Håkan började i Göteborgs högre Samskola bildade han ett band tillsammans med Henrik Berggren. Vad hette bandet?-Brodel Daniel-Brodel Håkan-Brodel Berggren-Brodel Daniel-Broder Daniel (BD) var en musikgrupp från Göteborg. Gruppen bildades 1989 och upplöstes 2008 efter att gitarristen Anders Göthberg avlidit den 30 mars 2008.";
        questions[7] = "Allen-Nya Allén i Göteborg omsjungen av storheter som Nationalteatern och Håkan. I låten 'Zigernaliv dreamin' nämner håkan Nya Allén 'Zigernaliv dreamin~Tar en sväng ner mot allén sätter mig en stund under träden allt är sig likt här sen Nationalteatern köper basiado av en kara på torget. Och går hem och lägger mig på sängen och läser Beppe Wolgers.'. Vad är Nya Allén för nånting?-En nöjespark-En parkgata-En arena-En parkgata-Allén en gata som sträcker sig cirka 1800 meter i centrala Göteborg. Allén stod klar år 1823. Idag knappt 200 år sedan är Allén den mest populära parken i Göteborg.";
        questions[8] = "Mug-Musik utan gränser, år 2008 under Håkan Hellströms 24 gig turné så var ett utav stoppen här på mug, samma år som han släppte albumet 'För sent för edelweiss'. Vad är musik utan gränser för nånting?-Ett stadium-En musikaffär-En studio-En musikaffär-Håkan praktiserade här i mug som är en musikaffär under sin skolgång i Samskolan. Drömmen för Håkan var att arbeta inom musikbranschen redan i tidigt ålder.";
        questions[9] = "Feskekorka-Feskekörka är en fisk marknad som har sålt fisk i över 140 år, där finns även en skaldjursrestaurang . Vid ena hörnet av kyrkan så finns det en container som en utav de bästa scenerna ur en film spelades, den är även omtalad i Håkans texter. Vad heter filmen som utspelade sina scener vid hörnet av kyrkan?-Glädje och sorg-Ingen glädje utan sorg-Känn ingen sorg-Känn ingen sorg-Känn ingen sorg är en Guldbaggebelönad svensk långfilm som hade biopremiär 19 juli 2013. Filmen är baserad på Håkan Hellströms låtar. Manuset skrevs av Cilla Jackert och filmen regisserades av Måns Mårlind och Björn Stein.";
        questions[10] = "Stibergsliden-Stigbergsliden sträcker sig ca 350m från Johannesplatsen upp till Stigbergstorget och nämns i 'Zigenarliv Dreamin' från albumet 'För sent för Edelweiss'. Vad är det första Håkan sjunger i låten Zigenarliv Dreamin från albumet?-En vacker dag på Stigbergsliden-En underbar dag på Stigbergsliden-En solig dag på Stigbergsliden-En vacker dag på Stigbergsliden-'En vacker dag på Stigbergsliden ser upp och ser en fågel o tänker 'din jävel'. Texten är tagen ur låten 'Zigenarliv Dreamin'.";
        questions[11] = "Gullbergskaj-Kajen belägen i stadsdelen Gullbergsvass, är en plats som Håkan håller kär. Låten 'Mitt Gullbergs kaj paradis' är inspirerad från denna plats. Kajen benämns också som ett annat namn för att platsen ej är moderniserad. Det vackra med denna plats är att den har stått stilla i flera år. Vilket är det andra namnet för GullbergsKajen?-Paradisets kaj-Drömmarnas kaj-Orörda kaj-Drömmarnas kaj-Kajen benämns också Drömmarnas kaj och där ligger en mängd äldre fartyg under renovering. Längs kajen finns även ett flytande hotell.";
        questions[12] = "Bageriet-I albumet 'För Sent För Edelweiss' sjunger Håkan om ett bageri. Albumet släpptes år 1994. Håkan arbetade på bageriet tills han blev en trummis i ett band från 1995 till 1997. Vad heter bandet som Håkan blev trummis i?-Honey Is sweet-Honey Is Cool-Honey is in love-Honey Is Cool-Honey Is Cool var ett indiepopband från Göteborg som hade sin storhetstid under senare hälften av 1990talet. Deras musik kännetecknades bland annat av sångerskans säregna och lite juvenila röst. Gruppen bildades 1994 och släppte sin sista skiva år 2000. En utav medlemmarna var Håkan Hellström.";
        questions[13] = "Jazzhuset-'Ja, Jazzhuset är fullt med trix och cocacola chicks och kicks!'. Detta nämner Håkan i sin hitlåt. Jazzhuset är en viktig del i Göteborgs nöjesliv sedan 1977. Tidigare var det enbart en Jazzklubb men nuförtiden så huserar allt från Indiepop, garagerock synt och electronica på klubbarna. Vad heter låten som blev Håkan Hellströms hitlåt?-Kom igen nu Lena-Kom igen nu-Kom igen Lena-Kom igen Lena-Hiten 'Kom igen Lena'  är en klassiker av Håkan som slog igenom 2002 från albumet 'Det är så jag säger det'.";
        assertNotNull(questions);
        assertTrue(questions.length == 14);

        for (int i = 0; i < questions.length; i++) {
            String question[] = questions[i].split("-");
            Quiz quiz = new Quiz();
            quiz.setTitle(question[0]);
            quiz.setQuestion(question[1]);
            quiz.setAlt1(question[2]);
            quiz.setAlt2(question[3]);
            quiz.setAlt3(question[4]);
            quiz.setAnswer(question[5]);
            quiz.setOnAnswer(question[6]);

            assertNotNull(quiz);

            questionListThree.add(quiz);
        }
        assertNotNull(questionListThree);
    }
}

