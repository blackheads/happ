package com.groupsex.testproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.groupsex.testproject.repository.Helper;
import com.groupsex.testproject.repository.Landmark;
import com.groupsex.testproject.repository.LandmarkMockup;
import com.groupsex.testproject.repository.Quiz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class FootsparActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        android.location.LocationListener {

    private boolean mCompleted;
    private int mRightAnswerCount;
    private int mTotalAnswerCount;
    private ArrayList<Landmark> mLandmarks;
    private ArrayList<Quiz> mQuizArrayList;
    private Set<String> mTitles;
    private GoogleApiClient mGoogleApiClient;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private MapView mMapView;
    private GoogleMap mMap;
    private String mProvider;
    private LinearLayout mPopup;
    private Marker mMarker;
    private SharedPreferences mSharedPreferences;
    private TextView mTextViewScore;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.footspar_layout);

        if (getActionBar() != null)
            getActionBar().setDisplayShowHomeEnabled(false);

        LandmarkMockup landmarkMockup = new LandmarkMockup();
        if (mLandmarks == null)
            mLandmarks = landmarkMockup.getLandmarks();
        mTitles = new HashSet<>();
        mCompleted = false;
        mPopup = (LinearLayout) findViewById(R.id.popup);
        mQuizArrayList = Helper.getQuestions();
        Criteria criteria = new Criteria();
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mProvider = mLocationManager.getBestProvider(criteria, false);
        mPopup.setVisibility(View.INVISIBLE);
        loadValues();
        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMap = mMapView.getMap();
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                return false;
            }
        });

        setMapView();
        buildGoogleApiClient();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000).setFastestInterval(10 * 1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mTextViewScore = (TextView) findViewById(R.id.score);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/HussarBold.otf");
        mTextViewScore.setTypeface(face);
        mTextViewScore.setPadding(0, 0, 20, 0);
        mTextViewScore.setText("SCORE: " + mRightAnswerCount + "/ " + mTotalAnswerCount);
        mTextViewScore.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mTextViewScore.setTextSize(16);
        menu.add("").setActionView(mTextViewScore).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    /**
     * Checks if Google Play Services is available.
     * If available it gets a locationManager to update the locations.
     * Checks if the map is there, if not sends an error toast to the user.
     * Starts the addMarker method to add all the markers on the map.
     * Sets the icon on the map "my location" to click and get to your location and Sets the map type to normal.
     * If other errors occurs it handles them.
     */
    private void setMapView() {
        try {
            Dialog dialog;
            MapsInitializer.initialize(this);
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            switch (resultCode) {
                case ConnectionResult.SUCCESS:
                    if (mMapView != null) {
                        mLocationManager = ((LocationManager) getSystemService(Context.LOCATION_SERVICE));
                        mMap = mMapView.getMap();
                        if (mMap == null) {
                            Toast.makeText(this, "Map Not Found", Toast.LENGTH_SHORT).show();
                        }
                        mMap.clear();
                        try {
                            addMarkers(mMap);
                        } catch (Exception e) {
                            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
                        }
                        mMap.setIndoorEnabled(true);
                        mMap.setMyLocationEnabled(true);
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                    break;
                case ConnectionResult.SERVICE_MISSING:
                    dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 1);
                    dialog.show();
                    break;
                case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                    dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 1);
                    dialog.show();
                    break;
                case ConnectionResult.SERVICE_DISABLED:
                    dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 1);
                    dialog.show();
                default:
                    // TODO
                    break;
            }
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Populates the map with markers. Gets the cordinates of the markers from landmark class.
     * Adds an icon to every marker, sets a cirkel zone around every marker.
     * @param map the googleMap.
     */
    private void addMarkers(GoogleMap map) {
        for (Landmark landmark : mLandmarks) {
            mMarker = map.addMarker(landmark.getMarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon_smaller))
                    .position(landmark.getPosition())
                    .title(landmark.getTitle()));
            map.addCircle(landmark.getCircleOptions());
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        } else {
            zoomInOnMyLocation(location);
            handleNewLocation(location);
        }
    }
    protected final synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * The first thing it checks is if the user has been on all the 14 markers on the map.
     * If so the user has completed the game and gets a popup with the score.
     * If not, it checks whether the user has reached a marker zone or not.
     * If the user har reached one, it loops the coordinates of the landmarks
     * and checks which landmark the user is on.
     * After that it checks if the landmark has been visited before, if not
     * the user gets a popup with the quizwalk question specific for that landmark.
     * With three buttons containing different answers.
     * After a answer has been clicked. The user has completed the answer for this landmark
     * so the marker color sets to green.
     * @param location
     */
    private void handleNewLocation(Location location) {
        mQuizArrayList = Helper.getQuestions();
        float[] distance = new float[2];
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        TextView circleTitle = (TextView) findViewById(R.id.title);
        final TextView circleQuestion = (TextView) findViewById(R.id.question);
        final Button btnAnswer_1 = (Button) findViewById(R.id.btnAlt_1);
        final Button btnAnswer_2 = (Button) findViewById(R.id.btnAlt_2);
        final Button btnAnswer_3 = (Button) findViewById(R.id.btnAlt_3);
        if (mTitles.size() == 14) {
            mCompleted = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setTitle("Gratulerar!")
                    .setMessage("Du fick " + mRightAnswerCount + " rätt av 14. Vill du gå tipspromenaden igen?")
                    .setPositiveButton("JA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            resetGame();
                        }
                    })
                    .setNegativeButton("NEJ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(FootsparActivity.this);
                            builder
                                    .setTitle("Tack!")
                                    .setMessage("För att du spelade!")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            resetGame();
                                            System.exit(0);
                                        }
                                    });
                            AlertDialog dialog1 = builder.create();
                            dialog1.show();
                        }
                    });
            Dialog d = builder.show();
            int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
            TextView tv = (TextView) d.findViewById(textViewId);
            tv.setTextColor(getResources().getColor(R.color.primaryColor));
            int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = d.findViewById(dividerId);
            divider.setBackgroundColor(getResources().getColor(R.color.primaryColor));
        } else {
            for (int i = 0; i < mLandmarks.size(); i++) {
                final Landmark landmark = mLandmarks.get(i);
                Location.distanceBetween(currentLatitude, currentLongitude,
                        landmark.getCircleOptions().getCenter().latitude,
                        landmark.getCircleOptions().getCenter().longitude, distance);
                if (distance[0] < landmark.getCircleOptions().getRadius()
                        && !mTitles.contains(landmark.getTitle())) {
                    mPopup.setVisibility(View.VISIBLE);
                    final Quiz quiz = mQuizArrayList.get(i);
                    circleTitle.setText(quiz.getTitle());
                    circleQuestion.setText(quiz.getQuestion());
                    btnAnswer_1.setText(quiz.getAlt1());
                    btnAnswer_2.setText(quiz.getAlt2());
                    btnAnswer_3.setText(quiz.getAlt3());

                    btnAnswer_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            buttonOnClick(btnAnswer_1, quiz, landmark);
                        }
                    });

                    btnAnswer_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            buttonOnClick(btnAnswer_2, quiz, landmark);
                        }
                    });

                    btnAnswer_3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            buttonOnClick(btnAnswer_3, quiz, landmark);
                        }
                    });

                    mMarker = mMap.addMarker(landmark.getMarkerOptions());

                } else {
                    if (mTitles.contains(landmark.getTitle())) {
                        mMarker = mMap.addMarker(landmark.getMarkerOptions());
                        mMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon_smaller_green));
                    }
                }
            }
        }
    }

    /**
     * Checks which button that contains an answer has been pressed and validates if the answer is right.
     * The title of the landmark gets saved. For checkup afterwards that the user already visited the landmark.
     * Afterwards a popup message presents so that the user knows if it has answered right or wrong.
     * If right, a counter adds points.
     * And at last the total points and the number of answered questions get displayed for the user.
     *
     * @param btn The button that has been clicked.
     * @param quiz the quiz that has been answered.
     * @param landmark the landmark that has been visited.
     */
    private void buttonOnClick(Button btn, Quiz quiz, Landmark landmark) {
        mTotalAnswerCount++;
        mTitles.add(landmark.getTitle());
        if (btn.getText().toString().equalsIgnoreCase(quiz.getAnswer())) {
            mRightAnswerCount++;
            mPopup.setVisibility(View.INVISIBLE);
            AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
            showPopup(builder, "RÄTT SVAR", quiz.getOnAnswer(), R.color.primaryColor, R.drawable.thumb_positive);
        } else {
            mPopup.setVisibility(View.INVISIBLE);
            AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
            showPopup(builder, "FEL SVAR", quiz.getOnAnswer(), R.color.accent, R.drawable.thumb_negative);
        }
        mTextViewScore.setText("Poäng: " + mRightAnswerCount + " av " + mTotalAnswerCount);
    }

    /**
     * Builds an AlerDialog with the arguments: title, message , color and icon.
     *
     * @param builder Contains the Alertdialog Builder.
     * @param title Contains a string with the title.
     * @param message Contains the string message.
     * @param color Contains the int textColor.
     * @param icon Contains a icon.
     */
    private void showPopup(AlertDialog.Builder builder, String title, String message, int color, int icon) {
        builder
                .setIcon(icon)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        Dialog d = builder.show();
        int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
        TextView tv = (TextView) d.findViewById(textViewId);
        tv.setTextColor(getResources().getColor(color));
        int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        View divider = d.findViewById(dividerId);
        divider.setBackgroundColor(getResources().getColor(color));
    }

    /**
     * Zooms in to the location of the user and sets the zoom level to 13.
     * @param location Contains the location, Latitude & longitude of the user.
     */
    private void zoomInOnMyLocation(Location location) {
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Location Services Suspended to Service, please reconnect",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
                connectionResult.startResolutionForResult(this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Location Service Connection Failed with Code: " +
                    connectionResult.getErrorCode(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
        mLocationManager.requestLocationUpdates(mProvider, 1000, 1, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
        visitedLandmarkTitles(mTitles);
        saveValues("score", mRightAnswerCount);
        saveValues("userTryCount", mTotalAnswerCount);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        loadVisitedLandmarkTitles();
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Aktivera GPS Location för att använda denna funktion",
                Toast.LENGTH_SHORT).show();
        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
    }



    /**
     * Resets everything that has been saved in sharedPreference
     * and restores the map to the starting point.
     */
    private void resetGame() {
        mTotalAnswerCount = 0;
        mRightAnswerCount = 0;
        mTitles = new HashSet<>();
        mTextViewScore.setText("Poäng: " + mRightAnswerCount + " av " + mTotalAnswerCount);
        SharedPreferences preferences = getSharedPreferences("PREFERENCE", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        addMarkers(mMap);
    }

    /**
     * Saves the score after the user has responded to the answer.
     * Saves the counts of answered questions.
     * And saves the boolean if the game is completed or not.
     * @param score Contains the score value.
     * @param source Cointains the counts of answered questions.
     */
    private void saveValues(String score, int source) {
        source++;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(score, source);
        editor.putBoolean("completed", mCompleted);
        editor.apply();
    }

    /**
     * Loads the score values from sharedPreference
     */
    private void loadValues() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mRightAnswerCount = mSharedPreferences.getInt("score", 0);
        mTotalAnswerCount = mSharedPreferences.getInt("userTryCount", 0);
        mCompleted = mSharedPreferences.getBoolean("completed", false);
    }

    /**
     * Saves the titles of the landmarks that has been visisted into sharedPreference
     * @param titles Represent the title of the landmark
     */
    private void visitedLandmarkTitles(Set<String> titles) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putStringSet("mTitles", titles);
        editor.apply();
    }


    /**
     * Loads the landmarks from sharedpreference.
     */
    private void loadVisitedLandmarkTitles() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mTitles = mSharedPreferences.getStringSet("mTitles", mTitles);
    }
}