package com.groupsex.testproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.groupsex.testproject.repository.Helper;

/**
 *
 * This is a fan application dedicated to Swedish popstar H�kan Hellstr�m. Users can use this
 * application to take a dynamic quiz about the popstar.
 *
 * HAPP is a very simple application. It contains only two activities. One for home screen and
 * the other for the quiz game. H�kan Hellstr�m spend most of his life in the city of Gothenburg,
 * and he left his footprints all over the city. On the quiz page users will see several red
 * markers in the city of Gothenburg. Those places contains unforgettable memories. When a user/fan
 * is in the circle of a red marker, a question about the place and its relation to H�kan Hellstr�m
 * will be appear. Right answer will give the user/fan a point. Users/fan will have only one chance
 * to answer for a specific place. Marker colors on visited places turns into green and no activated
 * quiz for those places untill a new game. When a user/fan completes the whole city markers, he or
 * she will be prompted his/her total score and will be learned much more about all those places and
 * about H�kan Hellstr�m himself.
 *
 * @author Kasim & Mavve & Babak & Roner - Project for Produce and Deliver Software, TeknikHogskolan
 */
public class MainActivity extends Activity {

    // A Simple welcome page contains only a call to action button and a link to a help guide.
    Button mCtaButton;
    TextView helpDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Helper.setContext(getBaseContext());
        helpDialog = (TextView) findViewById(R.id.help_popup);
        mCtaButton = (Button) findViewById(R.id.cta_button);
        mCtaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FootsparActivity.class);
                startActivity(intent);
            }
        });
        helpDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = getResources().getString(R.string.titel_help);
                String text = getResources().getString(R.string.help_game);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder
                        .setTitle(title)
                        .setMessage(text)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog d = builder.create();
                d.show();
            }
        });
    }
}
