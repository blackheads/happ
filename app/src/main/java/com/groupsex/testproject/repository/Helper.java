package com.groupsex.testproject.repository;

import android.content.Context;
import android.util.Log;

import com.groupsex.testproject.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

/**
 * {@code Helper} class is an utility class to used for to get quiz questions from multiple
 * resource files under {@code raw} folder. It contains only two methods {@code setContext}
 * and {@code getQuestions}.
 *
 * @author Kasim & Mavve & Babak & Roner - Project for Produce and Deliver Software, TeknikHogskolan
 */

public class Helper {

    private static final String TAG = Helper.class.getSimpleName();
    private static Context sContext;

    public static void setContext(Context context) {
        if (sContext == null)
            sContext = context;
    }

    public static ArrayList<Quiz> getQuestions() {
        Random random = new Random();
        int selectQuestion = random.nextInt(3) + 1;
        ArrayList<Quiz> quizList = new ArrayList<>();
        InputStream inputStream = null;
        InputStreamReader inputReader = null;
        BufferedReader bufferedReader;
        String line;
        try {
            switch(selectQuestion) {
                case 1:
                    inputStream = sContext.getResources().openRawResource(R.raw.quiz_walk_questions);
                    break;
                case 2:
                    inputStream = sContext.getResources().openRawResource(R.raw.quiz_walk_questions2);
                    break;
                case 3:
                    inputStream = sContext.getResources().openRawResource(R.raw.quiz_walk_questions3);
                    break;
            }
            inputReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputReader);
            while ((line = bufferedReader.readLine()) != null) {
                String[] quizDetails = line.split("-");
                Quiz quiz = new Quiz();
                quiz.setTitle(quizDetails[0]);
                quiz.setQuestion(quizDetails[1]);
                quiz.setAlt1(quizDetails[2]);
                quiz.setAlt2(quizDetails[3]);
                quiz.setAlt3(quizDetails[4]);
                quiz.setAnswer(quizDetails[5]);
                quiz.setOnAnswer(quizDetails[6]);
                quizList.add(quiz);
            }
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (inputReader != null) {
                    inputReader.close();
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return quizList;
    }
}
