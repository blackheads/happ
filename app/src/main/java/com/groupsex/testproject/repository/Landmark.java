package com.groupsex.testproject.repository;

import android.graphics.Color;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * {@code Landmark} class is an entity class used to hold information about every marked places
 * on the map. It is a domain object. It is used to establish a mapping between {@code Landmark}
 * object and a list of {@code LandmarkMockup} object.
 *
 * @author Kasim & Mavve & Babak & Roner - Project for Produce and Deliver Software, TeknikHogskolan
 */
public class Landmark {

    private String title;
    private LatLng position;
    private MarkerOptions markerOptions;
    private CircleOptions circleOptions;

    public Landmark(String title, LatLng position) {
        this.title = title;
        this.position = position;
        markerOptions = new MarkerOptions();
        circleOptions = new CircleOptions();
        circleOptions.center(position).radius(40).fillColor(Color.argb(80, 00, 99, 00))
                .strokeColor(Color.TRANSPARENT);
    }

    public String getTitle() {
        return title;
    }

    public LatLng getPosition() {
        return position;
    }

    public MarkerOptions getMarkerOptions() {
        return markerOptions;
    }

    public CircleOptions getCircleOptions() {
        return circleOptions;
    }

    @Override
    public String toString() {
        return "Landmark{" +
                "title='" + title + '\'' +
                ", position=" + position +
                ", markerOptions=" + markerOptions +
                ", circleOptions=" + circleOptions +
                '}';
    }
}
