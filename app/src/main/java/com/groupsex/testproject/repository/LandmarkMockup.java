package com.groupsex.testproject.repository;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * {@code Helper} class is an utility class which used to get {@code Landmark} objects and create
 * an ArrayList with them. {@code Singleton} design pattern is used for this particular class in
 * order to prevent to have problem with multiple list och Landmarks.
 *
 * @author Kasim & Mavve & Babak & Roner - Project for Produce and Deliver Software, TeknikHogskolan
 */
public class LandmarkMockup {

    private ArrayList<Landmark> landmarks = new ArrayList<>();

    /**
     * Users can access the list of {@code Landmark} object only through this method. If a list
     * already created before then it will be return. Otherwise a new list will be created from
     * {code createLandmarks} method.
     *
     * @return an ArrayList of {@code Landmark} objects.
     */
    public ArrayList<Landmark> getLandmarks() {
        if (landmarks.isEmpty()) {
            createLandmarks();
        }
        return landmarks;
    }

    private void createLandmarks() {
        Landmark ullevi = new Landmark("Ullevi", new LatLng(57.70635984, 11.98640585));
        Landmark paviljong = new Landmark("L\u00e5ngedrags Paviljong", new LatLng(57.66846, 11.84899));
        Landmark hakansuppvaxt = new Landmark("H\u00e5kans uppv\u00e4xt", new LatLng(57.676324, 11.90709));
        Landmark dalaskolan = new Landmark("Dalaskolan", new LatLng(57.67877, 11.90114));
        Landmark valvet = new Landmark("Valvet", new LatLng(57.6998825, 11.9497953));
        Landmark slottskogsvallen = new Landmark("Slottskogsvallen", new LatLng(57.677758, 11.93958));
        Landmark samskolan = new Landmark("G\u00f6teborgs H\u00f6gre Samskola", new LatLng(57.711926, 11.990583));
        Landmark allen = new Landmark("Allén", new LatLng(57.700893, 11.967701));
        Landmark mug = new Landmark("MUG", new LatLng(57.7024563, 11.9616408));
        Landmark feskekorka = new Landmark("Feskek\u00f6rka", new LatLng(57.7009921, 11.9579404));
        Landmark stibergsliden = new Landmark("Stibergsliden", new LatLng(57.699539, 11.936421));
        Landmark gullbergskaj = new Landmark("Gullbergskaj", new LatLng(57.717973, 11.980164));
        Landmark bageriet = new Landmark("P\u00e5\u00e5ls Bageri (P\u00e5gen)", new LatLng(57.6610283, 11.9386589));
        Landmark jazzhuset = new Landmark("Jazzhuset", new LatLng(57.6994931, 11.967943));

        landmarks.add(paviljong);
        landmarks.add(hakansuppvaxt);
        landmarks.add(dalaskolan);
        landmarks.add(ullevi);
        landmarks.add(valvet);
        landmarks.add(slottskogsvallen);
        landmarks.add(samskolan);
        landmarks.add(allen);
        landmarks.add(mug);
        landmarks.add(feskekorka);
        landmarks.add(stibergsliden);
        landmarks.add(gullbergskaj);
        landmarks.add(bageriet);
        landmarks.add(jazzhuset);
    }
}
