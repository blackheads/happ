package com.groupsex.testproject.repository;

/**
 * {@code Quiz} class is an entity class which used to hold details about every questions. It is
 * a domain object.
 *
 * @author Kasim & Mavve & Babak & Roner - Project for Produce and Deliver Software, TeknikHogskolan
 */
public class Quiz {

    private String onAnswer;
    private String title;
    private String question;
    private String alt1;
    private String alt2;
    private String alt3;
    private String answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAlt1() {
        return alt1;
    }

    public void setAlt1(String alt1) {
        this.alt1 = alt1;
    }

    public String getAlt2() {
        return alt2;
    }

    public void setAlt2(String alt2) {
        this.alt2 = alt2;
    }

    public String getAlt3() {
        return alt3;
    }

    public void setAlt3(String alt3) {
        this.alt3 = alt3;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOnAnswer() {
        return onAnswer;
    }

    public void setOnAnswer(String onAnswer) {
        this.onAnswer = onAnswer;
    }
}